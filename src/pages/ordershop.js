import OrderShop from "@/components/views/OrderShop/OrderShop"
import SEO from "@/components/SEO"

export default function OrderShopPage() {
  return (
    <>
      <SEO />
      <OrderShop />
    </>
  )
}
