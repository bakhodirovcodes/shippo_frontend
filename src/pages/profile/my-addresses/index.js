import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import MyAddressesView from "@/components/views/Profile/MyAddressesView"

function MyAddresses() {
  return (
    <ProfileLayout>
      <MyAddressesView />
    </ProfileLayout>
  )
}

export default MyAddresses
