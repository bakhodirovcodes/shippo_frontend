import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import InUzbekistanView from "@/components/views/Profile/MyParcels/InUzbekistanView"

function InUzbekistan() {
  return (
    <ProfileLayout>
      <InUzbekistanView />
    </ProfileLayout>
  )
}

export default InUzbekistan
