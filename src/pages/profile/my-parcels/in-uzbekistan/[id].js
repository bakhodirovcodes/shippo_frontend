import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import InUzbekistanSingleView from "@/components/views/Profile/MyParcels/InUzbekistanSingleView"

function InUzbekistanSingle() {
  return (
    <ProfileLayout>
      <InUzbekistanSingleView />
    </ProfileLayout>
  )
}

export default InUzbekistanSingle
