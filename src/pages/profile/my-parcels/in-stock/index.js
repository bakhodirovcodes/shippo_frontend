import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import InStockView from "@/components/views/Profile/MyParcels/InStockView"

function InStock() {
  return (
    <ProfileLayout>
      <InStockView />
    </ProfileLayout>
  )
}

export default InStock
