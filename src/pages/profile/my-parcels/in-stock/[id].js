import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import InStockSingleView from "@/components/views/Profile/MyParcels/InStockSingleView"

function InStockSingle() {
  return (
    <ProfileLayout>
      <InStockSingleView />
    </ProfileLayout>
  )
}

export default InStockSingle
