import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import ReceivedSingleView from "@/components/views/Profile/MyParcels/ReceivedSingleView"

function ReceivedSingle() {
  return (
    <ProfileLayout>
      <ReceivedSingleView />
    </ProfileLayout>
  )
}

export default ReceivedSingle
