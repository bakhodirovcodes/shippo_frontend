import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import OnWayView from "@/components/views/Profile/MyParcels/OnWayView"

function OnWay() {
  return (
    <ProfileLayout>
      <OnWayView />
    </ProfileLayout>
  )
}

export default OnWay
