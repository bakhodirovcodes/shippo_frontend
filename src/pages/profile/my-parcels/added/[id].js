import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import AddedSingleView from "@/components/views/Profile/MyParcels/AddedSingleView"

function AddedSingle() {
  return (
    <ProfileLayout>
      <AddedSingleView />
    </ProfileLayout>
  )
}

export default AddedSingle
