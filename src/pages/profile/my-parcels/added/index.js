import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import AddedView from "@/components/views/Profile/MyParcels/AddedView"

function Added() {
  return (
    <ProfileLayout>
      <AddedView />
    </ProfileLayout>
  )
}

export default Added
