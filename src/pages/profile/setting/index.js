import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import SettingView from "@/components/views/Profile/SettingView"

function Setting() {
  return (
    <ProfileLayout>
      <SettingView />
    </ProfileLayout>
  )
}

export default Setting
