import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import NotificationSingleView from "@/components/views/Profile/NotificationSingleView"

function NotificationSingle() {
  return (
    <ProfileLayout>
      <NotificationSingleView />
    </ProfileLayout>
  )
}

export default NotificationSingle
