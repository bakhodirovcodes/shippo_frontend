import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import NotificationView from "@/components/views/Profile/NotificationView"

function Notification() {
  return (
    <ProfileLayout>
      <NotificationView />
    </ProfileLayout>
  )
}

export default Notification
