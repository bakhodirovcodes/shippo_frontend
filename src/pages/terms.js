import TermsLink from "@/components/UI/Links/TermsLink/TermsLink"
import SEO from "@/components/SEO"

export default function GeneralTerms() {
  return (
    <>
      <SEO />
      <TermsLink />
    </>
  )
}
