import About from "@/components/views/About/About"
import SEO from "@/components/SEO"

export default function AboutUs() {
  return (
    <>
      <SEO />
      <About />
    </>
  )
}
