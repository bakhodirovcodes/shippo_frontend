import SEO from "@/components/SEO"
import StoresPage from "@/components/views/Stores/StoresPage"

export default function Stores() {
  return (
    <>
      <SEO />
      <StoresPage />
    </>
  )
}
