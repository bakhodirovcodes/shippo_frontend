import QuestionLink from "@/components/UI/Links/QuestionLink/QuestionLink"
import SEO from "@/components/SEO"

export default function FAQLinks() {
  return (
    <>
      <SEO />
      <QuestionLink />
    </>
  )
}
