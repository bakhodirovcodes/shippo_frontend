import SEO from "@/components/SEO"
import FAQ from "@/components/views/FAQ/FAQ"

export default function FAQPage() {
  return (
    <>
      <SEO />
      <FAQ />
    </>
  )
}
