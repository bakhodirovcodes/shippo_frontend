import ProhibitedLink from "@/components/UI/Links/ProhibitedLink/ProhibitedLink"
import SEO from "@/components/SEO"

export default function Prohibited() {
  return (
    <>
      <SEO />
      <ProhibitedLink />
    </>
  )
}
