import React, { useState } from "react";
import Modal from "@mui/material/Modal";
import cls from "./styles.module.scss";

const SuperModal = ({
  open,
  onClose,
  width = 550,
  isCommentAvailable = true,
}) => {
  const [modal, setModal] = useState(false);

  return (
    <div>
      <Modal
        open={open}
        onClose={onClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <div className={cls.modal} style={{ width: width }}>
          <div className={cls.headSection}>
            <img
              src="icons/boxIcon.svg"
              alt="modal"
              className={cls.modalIcon}
            />
            <div className={cls.headInfo}>
              <h2 id="modal-modal-title" className={cls.header}>
                Text in a modal
              </h2>
              <p className={cls.price}>4$</p>
            </div>
          </div>
          <p id="modal-modal-description" className={cls.description}>
            Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
          </p>
          {isCommentAvailable && (
            <div className={cls.comment}>
              <p className={cls.commentText}>Комментарии</p>
              <input placeholder="Комментарии" className={cls.commentInput} />
            </div>
          )}
          <div className={cls.buttons}>
            <button onClick={() => setModal(false)} className={cls.cancelBtn}>
              Отменить
            </button>
            <button className={cls.saveBtn}>Добавить</button>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default SuperModal;
