import { useState } from "react"
import Box from "@mui/material/Box"
import ProfileSearchInput from "@/components/UI/ProfileSearchInput"
import ProfilePagination from "@/components/ProfilePagination"
import {
  ProfileTabs,
  ProfileTab,
  ProfileTabPanel,
} from "@/components/ProfileTabs"
import List from "./components/List"
import { notificationItems } from "../profileData"
import { CheckIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function NotificationView() {
  const [value, setValue] = useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Посылки в Узбекистане</h2>

      <ProfileSearchInput />

      <Box sx={{ width: "100%" }}>
        <Box
          sx={{
            borderBottom: 1,
            borderColor: "divider",
            marginBottom: "1rem",
          }}
        >
          <ProfileTabs
            value={value}
            onChange={handleChange}
            aria-label="table tabs"
          >
            <ProfileTab label="Непрочитанные" a11yProps={0} />
            <ProfileTab label="Все уведомления" a11yProps={1} />

            <div className={styles.tabs__action}>
              <span className={styles.tabs__actionIcon}>
                <CheckIcon />
              </span>
              <span>Отметить все как прочитанное</span>
            </div>
          </ProfileTabs>
        </Box>
        <ProfileTabPanel value={value} index={0}>
          <List data={notificationItems} />
        </ProfileTabPanel>
        <ProfileTabPanel value={value} index={1}>
          <List data={notificationItems} />
        </ProfileTabPanel>
      </Box>

      <ProfilePagination count={86} />
    </div>
  )
}

export default NotificationView
