import { styled } from "@mui/material/styles"
import MuiButton from "@mui/material/Button"
import { AddIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const Button = styled(MuiButton)({
  padding: "0.5rem 0.75rem",
  borderRadius: "99px",
  backgroundColor: "#000",
  boxShadow: "none",
  fontSize: "1rem",
  lineHeight: "1.5rem",
  color: "#FFF",
  "&:hover": {
    backgroundColor: "#000",
  },
})

function BalanceCards() {
  return (
    <div className={styles.balanceCards}>
      <div className={styles.balanceCards__item}>
        <div className={styles.balanceCards__itemWrapper}>
          <span className={styles.subtitle}>Ваш баланс</span>
          <span className={styles.title}>97 $</span>
        </div>
        <Button variant="contained" startIcon={<AddIcon />}>
          Пополнить
        </Button>
      </div>

      <div className={styles.balanceCards__item}>
        <div className={styles.balanceCards__itemWrapper}>
          <span className={styles.subtitle}>Бонусы</span>
          <span className={styles.title}>97 $</span>
        </div>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#1AC19D",
            "&:hover": {
              backgroundColor: "#1AC19D",
            },
          }}
        >
          Списать бонусы
        </Button>
      </div>
    </div>
  )
}

export default BalanceCards
