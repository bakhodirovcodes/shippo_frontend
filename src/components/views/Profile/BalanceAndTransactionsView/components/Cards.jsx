import { useRouter } from "next/router"
import { PaymentIcon, ReplenishmentIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function Cards({ data }) {
  const router = useRouter()

  return (
    <div className={styles.cards}>
      {data.map(({ id, title, subtitle, amount, date, type }) => (
        <div key={id} className={styles.card}>
          <div>
            {type === "payment" ? <PaymentIcon /> : <ReplenishmentIcon />}
          </div>
          <div className={styles.card__innerWrapper}>
            <span>{title}</span>
            <span>{subtitle}</span>
          </div>
          <div className={styles.card__innerWrapper}>
            <span>{amount}</span>
            <span>{date}</span>
          </div>
        </div>
      ))}
    </div>
  )
}

export default Cards
