import Cards from "./components/Cards"
import BalanceCards from "./components/BalanceCards"
import ProfilePagination from "@/components/ProfilePagination"
import { balanceAndTransactionsItems } from "../profileData"
import styles from "./styles.module.scss"

function BalanceAndTransactionsView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Баланс и транзакции</h2>
      <BalanceCards />
      <Cards data={balanceAndTransactionsItems} />
      <ProfilePagination count={86} />
    </div>
  )
}

export default BalanceAndTransactionsView
