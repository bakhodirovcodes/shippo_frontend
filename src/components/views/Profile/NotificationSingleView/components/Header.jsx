import { useRouter } from "next/router"
import { ArrowRightIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function Header() {
  const router = useRouter()

  return (
    <div className={styles.header}>
      <span className={styles.header__action} onClick={() => router.back()}>
        <ArrowRightIcon />
      </span>
      <h4 className={styles.header__trackCode}>
        Ваша посылка уже на нашем международном складе
      </h4>
      <span className={styles.header__date}>22.01.2023 10:55</span>
    </div>
  )
}

export default Header
