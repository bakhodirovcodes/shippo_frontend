export const addedItems = {
  columns: [
    { id: "column-01", label: "Трек код" },
    { id: "column-02", label: "Наименование" },
    { id: "column-03", label: "Стоимость" },
    { id: "column-04", label: "Добавленние" },
    { id: "column-05", label: "" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "2ZDC23MCSLD",
      name: "Куртка",
      price: "126 $",
      date: "12.01.2023",
    },
    {
      id: "row-02",
      trackCode: "2ZDC23MCSLD",
      name: "Куртка женская",
      price: "126 $",
      date: "12.01.2023",
    },
    {
      id: "row-03",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      date: "12.01.2023",
    },
    {
      id: "row-04",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      date: "12.01.2023",
    },
    {
      id: "row-05",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      date: "12.01.2023",
    },
  ],
}

export const addedSingleItems = {
  columns: [
    { id: "column-01", label: "Наименование" },
    { id: "column-02", label: "Количество" },
    { id: "column-03", label: "Заметка" },
    { id: "column-04", label: "Стоимость" },
    { id: "column-05", label: "Сумма" },
  ],
  rows: [
    {
      id: "row-01",
      name: "Рубашка",
      quantity: 3,
      note: "Заметка для пользователя",
      price: "126 $",
      amount: "126 $",
    },
    {
      id: "row-02",
      name: "Рубашка",
      quantity: 2,
      note: "Заметка для пользователя",
      price: "126 $",
      amount: "126 $",
    },
    {
      id: "row-03",
      name: "Рубашка",
      quantity: 1,
      note: "Заметка для пользователя",
      price: "126 $",
      amount: "126 $",
    },
  ],
}

export const inStockItems = {
  columns: [
    { id: "column-01", label: "Трек код" },
    { id: "column-02", label: "Наименование" },
    { id: "column-03", label: "Стоимость услуги" },
    { id: "column-04", label: "Стоимость доставки" },
    { id: "column-05", label: "Дата получение на складе" },
    { id: "column-06", label: "Ожидаемая дата" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      priceDelivery: "126 $",
      recieveDate: "12.01.2023",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-02",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      priceDelivery: "126 $",
      recieveDate: "12.01.2023",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-03",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      priceDelivery: "126 $",
      recieveDate: "12.01.2023",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-04",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      priceDelivery: "126 $",
      recieveDate: "12.01.2023",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-05",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      priceDelivery: "126 $",
      recieveDate: "12.01.2023",
      expectedDate: "12.01.2023",
    },
  ],
}

export const onWayItems = {
  columns: [
    { id: "column-01", label: "Трек код" },
    { id: "column-02", label: "Наименование" },
    { id: "column-03", label: "Стоимость" },
    { id: "column-04", label: "Ожидаемая дата" },
    { id: "column-05", label: "" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "2ZDC23MCSLD",
      name: "Куртка",
      price: "126 $",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-02",
      trackCode: "2ZDC23MCSLD",
      name: "Куртка женская",
      price: "126 $",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-03",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-04",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      expectedDate: "12.01.2023",
    },
    {
      id: "row-05",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      expectedDate: "12.01.2023",
    },
  ],
}

export const inStockDeliveryCenterItems = {
  columns: [
    { id: "column-01", label: "Трек код" },
    { id: "column-02", label: "Получатель" },
    { id: "column-03", label: "Стоимость услуги" },
    { id: "column-04", label: "Посылка в стране" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "2ZDC23MCSLD",
      recipient: "Мере Мамажонова",
      priceDelivery: "126 $",
      packageDate: "до 12.01.2023",
    },
    {
      id: "row-02",
      trackCode: "2ZDC23MCSLD",
      recipient: "Мере Мамажонова",
      priceDelivery: "126 $",
      packageDate: "до 12.01.2023",
    },
    {
      id: "row-03",
      trackCode: "2ZDC23MCSLD",
      recipient: "Мере Мамажонова",
      priceDelivery: "126 $",
      packageDate: "10.01.2023 13:00",
    },
    {
      id: "row-04",
      trackCode: "2ZDC23MCSLD",
      recipient: "Мере Мамажонова",
      priceDelivery: "126 $",
      packageDate: "09.01.2023 14:00",
    },
    {
      id: "row-05",
      trackCode: "2ZDC23MCSLD",
      recipient: "Мере Мамажонова",
      priceDelivery: "126 $",
      packageDate: "до 12.01.2023",
    },
  ],
}

export const inStockDeliveryItems = {
  columns: [
    { id: "column-01", label: "Адрес" },
    { id: "column-02", label: "Статус" },
    { id: "column-03", label: "Кол-во" },
    { id: "column-04", label: "Заказ будет передан  курьеру" },
    { id: "column-05", label: "" },
  ],
  rows: [
    {
      id: "row-01",
      adress: "Караганда 49/6. 116а",
      status: "В пути",
      amount: 4,
      date: "до 12.01.2023",
      paymentStatus: "not-paid",
    },
    {
      id: "row-02",
      adress: "Караганда 49/6. 116а",
      status: "Запрошено",
      amount: 3,
      date: "до 12.01.2023",
      paymentStatus: "not-paid",
    },
    {
      id: "row-03",
      adress: "Караганда 49/6. 116а",
      status: "В пути",
      amount: 2,
      date: "10.01.2023 13:00",
      paymentStatus: "paid",
    },
    {
      id: "row-04",
      adress: "Караганда 49/6. 116а",
      status: "В пути",
      amount: 3,
      date: "09.01.2023 14:00",
      paymentStatus: "not-paid",
    },
    {
      id: "row-05",
      adress: "Караганда 49/6. 116а",
      status: "В пути",
      amount: 3,
      date: "до 12.01.2023",
      paymentStatus: "paid",
    },
  ],
}

export const receivedItems = {
  columns: [
    { id: "column-01", label: "Трек код" },
    { id: "column-02", label: "Наименование" },
    { id: "column-03", label: "Получатель" },
    { id: "column-04", label: "Стоимость услуги" },
    { id: "column-05", label: "Заказ получен" },
    { id: "column-06", label: "Тип доставки" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      recipient: "Мере Мамажонова",
      recieveDate: "12.01.2023",
      deliveryType: "Самовывоз",
    },
    {
      id: "row-02",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      recipient: "Мере Мамажонова",
      recieveDate: "12.01.2023",
      deliveryType: "До дома",
    },
    {
      id: "row-03",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      recipient: "Мере Мамажонова",
      recieveDate: "12.01.2023",
      deliveryType: "До дома",
    },
    {
      id: "row-04",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      recipient: "Мере Мамажонова",
      recieveDate: "12.01.2023",
      deliveryType: "Самовывоз",
    },
    {
      id: "row-05",
      trackCode: "2ZDC23MCSLD",
      name: "Рубашка",
      price: "126 $",
      recipient: "Мере Мамажонова",
      recieveDate: "12.01.2023",
      deliveryType: "Самовывоз",
    },
  ],
}

export const notificationItems = [
  {
    id: "item-01",
    label: "Ваша посылка уже на нашем международном складе",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-02",
    label: "Трек номер введен",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-03",
    label: "Ваша посылка уже на отправлена",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-04",
    label: "Ваша посылка уже на нашем международном складе",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-05",
    label: "Ваша посылка уже на отправлена",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-06",
    label: "Ваша посылка уже на отправлена",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-07",
    label: "Заказ подтвержден",
    date: "22.01.2023 10:55",
  },
  {
    id: "item-08",
    label: "Ваша посылка уже на нашем международном складе",
    date: "22.01.2023 10:55",
  },
]

export const myAddressesInfoItems = [
  {
    id: "item-01",
    subtitle: "Adres Satır",
    title:
      "29 Ekim Caddesi, İstanbul Vizyon Park, 4. Plaza, 7. Kat 705 - BR106627",
  },
  {
    id: "item-02",
    subtitle: "İlçe",
    title: "Bahçelievler",
  },
  {
    id: "item-03",
    subtitle: "Semt / Mahalle",
    title: "Yenibosna Merkez Mahallesi",
  },
  {
    id: "item-04",
    subtitle: "Zip / Posta kodu",
    title: "34197",
  },
  {
    id: "item-05",
    subtitle: "Şehir (İl)",
    title: "İstanbul",
  },
  {
    id: "item-06",
    subtitle: "Cep Telefonu",
    title: "+905419472252",
  },
  {
    id: "item-06",
    subtitle: "Ülke",
    title: "Турция",
  },
]

export const recipientsItem = [
  {
    id: "item-01",
    name: "Мереке Мамажонова Дусенбеккызы",
    number: "+7054445558",
    city: "Ташкент",
    email: "example@gmail.com",
    address: "1 переулок Байзак батыра 5 А",
  },
  {
    id: "item-02",
    name: "Мереке Мамажонова Дусенбеккызы",
    number: "+7054445558",
    city: "Ташкент",
    email: "example@gmail.com",
    address: "1 переулок Байзак батыра 5 А",
  },
  {
    id: "item-03",
    name: "Мереке Мамажонова Дусенбеккызы",
    number: "+7054445558",
    city: "Ташкент",
    email: "example@gmail.com",
    address: "1 переулок Байзак батыра 5 А",
  },
]

export const balanceAndTransactionsItems = [
  {
    id: "item-01",
    title: "Оплата за услуги №2ZDC23MCSLD",
    subtitle: "Комментарии",
    amount: "- 87,6 $",
    date: "15.02.23",
    type: "payment",
  },
  {
    id: "item-02",
    title: "Пополнение баланса",
    subtitle: "Payme",
    amount: "+100 $",
    date: "15.02.23",
    type: "replenishment",
  },
  {
    id: "item-03",
    title: "Пополнение баланса",
    subtitle: "Payme",
    amount: "+100 $",
    date: "15.02.23",
    type: "replenishment",
  },
  {
    id: "item-04",
    title: "Оплата за услуги №2ZDC23MCSLD ",
    subtitle: "Комментарии",
    amount: "- 87,6 $",
    date: "15.02.23",
    type: "payment",
  },
  {
    id: "item-05",
    title: "Пополнение баланса",
    subtitle: "Payme",
    amount: "+100 $",
    date: "15.02.23",
    type: "replenishment",
  },
  {
    id: "item-06",
    title: "Оплата за услуги №2ZDC23MCSLD ",
    subtitle: "Комментарии",
    amount: "- 87,6 $",
    date: "15.02.23",
    type: "payment",
  },
]
