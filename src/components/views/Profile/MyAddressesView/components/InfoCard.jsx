import { CopyIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function InfoCard({ data: { id, subtitle, title } }) {
  return (
    <div key={id} className={styles.infoCard__card}>
      <div className={styles.infoCard__content}>
        <span className={styles.infoCard__subtitle}>{subtitle}</span>
        <span className={styles.infoCard__title}>{title}</span>
      </div>
      <div
        className={styles.infoCard__action}
        onClick={() => navigator.clipboard.writeText(title)}
      >
        <CopyIcon />
      </div>
    </div>
  )
}

export default InfoCard
