import { useRouter } from "next/router"
import { UserIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function Cards({ data }) {
  const router = useRouter()

  return (
    <div className={styles.cards}>
      {data.map(({ id, name, number, city, email, address }) => (
        <div key={id} className={styles.card}>
          <div className={styles.card__header}>
            <span className={styles.card__headerIcon}>
              <UserIcon />
            </span>
            <span>{name}</span>
          </div>
          <div className={styles.card__contacts}>
            <div className={styles.card__contact}>
              <span>Номер телефона:</span>
              <span>{number}</span>
            </div>
            <div className={styles.card__contact}>
              <span>Город:</span>
              <span>{city}</span>
            </div>
            <div className={styles.card__contact}>
              <span>e-mail:</span>
              <span>{email}</span>
            </div>
            <div className={styles.card__contact}>
              <span>Адрес:</span>
              <span>{address}</span>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default Cards
