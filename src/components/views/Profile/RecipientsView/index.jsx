import { styled } from "@mui/material/styles"
import MuiButton from "@mui/material/Button"
import { AddIcon } from "public/icons/icons2"
import Cards from "./components/Cards"
import { recipientsItem } from "../profileData"
import styles from "./styles.module.scss"

const Button = styled(MuiButton)({
  padding: "0.5rem 1.5rem",
  backgroundColor: "#1AC19D",
  boxShadow: "none",
  fontSize: "1rem",
  lineHeight: "1.5rem",
  color: "#FFF",
  "&:hover": {
    backgroundColor: "#1AC19D",
  },
})

function RecipientsView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Получатели</h2>
      <Cards data={recipientsItem} />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button variant="contained" startIcon={<AddIcon />}>
          Добавить получателя
        </Button>
      </div>
    </div>
  )
}

export default RecipientsView
