import ProfileSearchInput from "@/components/UI/ProfileSearchInput";
import Table from "./components/Table";
import ProfilePagination from "@/components/ProfilePagination";
import { receivedItems } from "../../profileData";
import styles from "./styles.module.scss";
import EmptyBox from "@/components/EmptyBox/EmptyBox";

function ReceivedView() {
  console.log(receivedItems);

  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Получено</h2>
      <ProfileSearchInput />
      {receivedItems?.rows ? <Table data={receivedItems} /> : <EmptyBox />}
      <ProfilePagination />
    </div>
  );
}

export default ReceivedView;
