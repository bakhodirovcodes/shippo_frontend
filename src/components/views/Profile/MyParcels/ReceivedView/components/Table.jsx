import { useRouter } from "next/router"
import {
  ProfileTableContainer,
  ProfileTable,
  ProfileTableHead,
  ProfileTableBody,
  ProfileTableRow,
  ProfileTableCell,
} from "@/components/ProfileTable"

function Table({ data: { columns, rows } }) {
  const router = useRouter()

  return (
    <ProfileTableContainer>
      <ProfileTable>
        <ProfileTableHead>
          <ProfileTableRow>
            {columns.map(({ id, label }) => (
              <ProfileTableCell key={id}>{label}</ProfileTableCell>
            ))}
          </ProfileTableRow>
        </ProfileTableHead>
        <ProfileTableBody>
          {rows.map(
            ({
              id,
              trackCode,
              name,
              price,
              recipient,
              recieveDate,
              deliveryType,
            }) => (
              <ProfileTableRow
                key={id}
                onClick={() => router.push(`${router.pathname}/${id}`)}
              >
                <ProfileTableCell>{trackCode}</ProfileTableCell>
                <ProfileTableCell>{name}</ProfileTableCell>
                <ProfileTableCell>{price}</ProfileTableCell>
                <ProfileTableCell>{recipient}</ProfileTableCell>
                <ProfileTableCell>{recieveDate}</ProfileTableCell>
                <ProfileTableCell>{deliveryType}</ProfileTableCell>
              </ProfileTableRow>
            )
          )}
        </ProfileTableBody>
      </ProfileTable>
    </ProfileTableContainer>
  )
}

export default Table
