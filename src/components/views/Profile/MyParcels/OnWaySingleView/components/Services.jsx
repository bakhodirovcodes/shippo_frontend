import styles from "./styles.module.scss"

function Services() {
  return (
    <div className={styles.services}>
      <h4 className={styles.services__title}>Специальные услуги для посылки</h4>
      <p className={styles.services__subtitle}>
        В эту посылку не добавлены специальные услуги. <button>Добавить</button>
      </p>
    </div>
  )
}

export default Services
