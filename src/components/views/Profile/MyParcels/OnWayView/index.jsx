import ProfileSearchInput from "@/components/UI/ProfileSearchInput";
import Table from "./components/Table";
import ProfilePagination from "@/components/ProfilePagination";
import { onWayItems } from "../../profileData";
import styles from "./styles.module.scss";
import EmptyBox from "@/components/EmptyBox/EmptyBox";

function OnWayView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Посылки в пути</h2>
      <ProfileSearchInput />
      {onWayItems?.rows ? <Table data={onWayItems} /> : <EmptyBox />}
      <ProfilePagination />
    </div>
  );
}

export default OnWayView;
