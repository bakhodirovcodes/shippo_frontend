import ProfileSearchInput from "@/components/UI/ProfileSearchInput";
import Table from "./components/Table";
import ProfilePagination from "@/components/ProfilePagination";
import { addedItems } from "../../profileData";
import styles from "./styles.module.scss";
import EmptyBox from "@/components/EmptyBox/EmptyBox";

function AddedView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Добавлена</h2>
      <ProfileSearchInput />
      {addedItems?.rows ? <Table data={addedItems} /> : <EmptyBox />}

      <ProfilePagination />
    </div>
  );
}

export default AddedView;
