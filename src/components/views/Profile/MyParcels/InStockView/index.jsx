import ProfileSearchInput from "@/components/UI/ProfileSearchInput";
import Table from "./components/Table";
import ProfilePagination from "@/components/ProfilePagination";
import { inStockItems } from "../../profileData";
import styles from "./styles.module.scss";
import EmptyBox from "@/components/EmptyBox/EmptyBox";

function InStockView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Посылки на складе</h2>
      <ProfileSearchInput />
      {inStockItems?.rows ? <Table data={inStockItems} /> : <EmptyBox />}

      <ProfilePagination />
    </div>
  );
}

export default InStockView;
