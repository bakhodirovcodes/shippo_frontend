import { useRouter } from "next/router"
import { ArrowRightIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function Header() {
  const router = useRouter()

  return (
    <div className={styles.header}>
      <span className={styles.header__action} onClick={() => router.back()}>
        <ArrowRightIcon />
      </span>
      <h4 className={styles.header__trackCode}>2ZDC23MCSLD</h4>
    </div>
  )
}

export default Header
