import Header from "./components/Header"
import InfoCards from "./components/InfoCards"
import DeclarationTable from "./components/DeclarationTable"
import Services from "./components/Services"
import Status from "./components/Status"
import { addedSingleItems } from "../../profileData"
import styles from "./styles.module.scss"

function AddedSingleView() {
  return (
    <div className={styles.parcel}>
      <Header />
      <InfoCards />
      <DeclarationTable data={addedSingleItems} />
      <Services />
      <Status />
    </div>
  )
}

export default AddedSingleView
