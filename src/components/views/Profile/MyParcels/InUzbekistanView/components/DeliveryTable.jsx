import { useRouter } from "next/router"
import classNames from "classnames"
import {
  ProfileTableContainer,
  ProfileTable,
  ProfileTableHead,
  ProfileTableBody,
  ProfileTableRow,
  ProfileTableCell,
} from "@/components/ProfileTable"
import styles from "./styles.module.scss"

const paymentStatusValues = {
  paid: "Оплачено",
  "not-paid": "Не оплачено",
}

function Table({ data: { columns, rows } }) {
  const router = useRouter()

  return (
    <ProfileTableContainer>
      <ProfileTable>
        <ProfileTableHead>
          <ProfileTableRow>
            {columns.map(({ id, label }) => (
              <ProfileTableCell key={id}>{label}</ProfileTableCell>
            ))}
          </ProfileTableRow>
        </ProfileTableHead>
        <ProfileTableBody>
          {rows.map(({ id, adress, status, amount, date, paymentStatus }) => (
            <ProfileTableRow
              key={id}
              onClick={() => router.push(`${router.pathname}/${id}`)}
            >
              <ProfileTableCell>{adress}</ProfileTableCell>
              <ProfileTableCell>{status}</ProfileTableCell>
              <ProfileTableCell>{amount}</ProfileTableCell>
              <ProfileTableCell>{date}</ProfileTableCell>
              <ProfileTableCell
                sx={{
                  "&.MuiTableCell-body": { padding: "0.25rem 0.75rem" },
                }}
              >
                <div
                  className={classNames(styles.table__paymentStatus, {
                    [styles.table__paymentStatus_paid]:
                      paymentStatus === "paid",
                  })}
                >
                  {paymentStatusValues[paymentStatus]}
                </div>
              </ProfileTableCell>
            </ProfileTableRow>
          ))}
        </ProfileTableBody>
      </ProfileTable>
    </ProfileTableContainer>
  )
}

export default Table
