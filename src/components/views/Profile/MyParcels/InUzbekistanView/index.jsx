import { useState } from "react";
import Box from "@mui/material/Box";
import ProfileSearchInput from "@/components/UI/ProfileSearchInput";
import DeliveryCenterTable from "./components/DeliveryCenterTable";
import DeliveryTable from "./components/DeliveryTable";
import ProfilePagination from "@/components/ProfilePagination";
import {
  ProfileTabs,
  ProfileTab,
  ProfileTabPanel,
} from "@/components/ProfileTabs";
import {
  inStockDeliveryCenterItems,
  inStockDeliveryItems,
} from "../../profileData";
import styles from "./styles.module.scss";
import EmptyBox from "@/components/EmptyBox/EmptyBox";

function InUzbekistanView() {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Посылки в Узбекистане</h2>

      <ProfileSearchInput />

      <Box sx={{ width: "100%" }}>
        <Box
          sx={{
            borderBottom: 1,
            borderColor: "divider",
            marginBottom: "1rem",
          }}
        >
          <ProfileTabs
            value={value}
            onChange={handleChange}
            aria-label="table tabs"
          >
            <ProfileTab label="Центр доставок" a11yProps={0} />
            <ProfileTab label="Доставка" a11yProps={1} />
          </ProfileTabs>
        </Box>
        <ProfileTabPanel value={value} index={0}>
          {inStockDeliveryCenterItems?.rows ? (
            <DeliveryCenterTable data={inStockDeliveryCenterItems} />
          ) : (
            <EmptyBox />
          )}
        </ProfileTabPanel>
        <ProfileTabPanel value={value} index={1}>
          {inStockDeliveryItems?.rows ? (
            <DeliveryTable data={inStockDeliveryItems} />
          ) : (
            <EmptyBox p="155px" />
          )}
        </ProfileTabPanel>
      </Box>

      <ProfilePagination />
    </div>
  );
}

export default InUzbekistanView;
