import FillingCard from "@/components/UI/Cards/FillingCard/FillingCard"
import CustomSections from "@/components/UI/CustomSections/CustomSection"
import styles from "./styles.module.scss"

const OrderShop = () => {
  return (
    <CustomSections changeColor={1}>
      <div className={styles.container}>
        <h1 className={styles.title}>Заказать покупку</h1>
        <FillingCard />
      </div>
    </CustomSections>
  )
}

export default OrderShop
