import SpecServicesCard from "@/components/SpecServicesCard/SpecServicesCard";
import SuperModal from "@/components/SuperModal/SuperModal";
import { Container } from "@mui/system";
import React, { useState } from "react";
import cls from "./styles.module.scss";

const products = [
  {
    id: 1,
    title: "Red T-shirt",
    price: "15$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 2,
    title: "Blue Hoodie",
    price: "30$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 3,
    title: "Black Jeans",
    price: "50$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 4,
    title: "Gray Sweatpants",
    price: "25$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 5,
    title: "White Sneakers",
    price: "70$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 6,
    title: "Green Jacket",
    price: "80$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 7,
    title: "Yellow Shorts",
    price: "20$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 8,
    title: "Pink Skirt",
    price: "35$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 9,
    title: "Orange Polo Shirt",
    price: "45$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 10,
    title: "Brown Boots",
    price: "90$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 11,
    title: "Purple Dress",
    price: "60$",
    disabled: false,
    isAvailable: true,
  },
  {
    id: 12,
    title: "Navy Blue Blazer",
    price: "100$",
    disabled: false,
    isAvailable: true,
  },
];

const SpecServices = () => {
  const [modalStatus, setModalStatus] = useState(false);
  return (
    <div className={cls.main}>
      <Container>
        <h1 className={cls.header}>Специальные услуги </h1>

        <div className={cls.services}>
          <h1
            className={cls.servicesHeader}
            onClick={() => setModalStatus(true)}
          >
            Услуги
          </h1>
          <div className={cls.serviceItems}>
            {products?.map((product) => (
              <SpecServicesCard key={product.id} />
            ))}
          </div>
        </div>
      </Container>

      <SuperModal open={modalStatus} onClose={() => setModalStatus(false)} />
    </div>
  );
};

export default SpecServices;
