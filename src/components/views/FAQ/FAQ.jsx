import CustomSections from "@/components/UI/CustomSections/CustomSection"
import DeliveryCount from "@/components/UI/Sections/DeliveryCount/DeliveryCount"
import ShippoService from "@/components/UI/Sections/ShippoService/ShippoService"
import HowItWork from "@/components/UI/Sections/HowItWork/HowItWork"
import ImportantForDelivery from "@/components/UI/Sections/ImportantForDelivery/ImportantForDelivery"
import styles from "./styles.module.scss"

const FAQ = () => {
  return (
    <div className={styles.container}>
      <CustomSections>
        <HowItWork />
      </CustomSections>
      <CustomSections changeColor={2}>
        <ShippoService />
      </CustomSections>
      <CustomSections changeColor={2}>
        <DeliveryCount />
      </CustomSections>
      <CustomSections>
        <ImportantForDelivery />
      </CustomSections>
    </div>
  )
}

export default FAQ
