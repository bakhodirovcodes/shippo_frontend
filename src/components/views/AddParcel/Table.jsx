import React from "react";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import DeleteIcon from "@mui/icons-material/Delete";
import { TableRow, Paper, TableCell, TableData } from "./addParcelItems";
import cls from "./tableStyles.module.scss";

const ParcelTable = ({ register, fields, remove }) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            {TableData?.columns?.map(({ id, label }) => (
              <TableCell key={id}>{label}</TableCell>
            ))}
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {fields?.map((field, index) => (
            <TableRow key={field.id}>
              <TableCell>
                <input
                  placeholder="Add label"
                  type="text"
                  {...register(`tableData.${index}.label`)}
                  className={cls.textInput}
                />
              </TableCell>
              <TableCell className={cls.checkboxCell}>
                <input
                  type="checkbox"
                  {...register(`tableData.${index}.isChecked`)}
                  className={cls.checkbox}
                />
              </TableCell>
              <TableCell>
                <input
                  placeholder="Add count"
                  type="text"
                  {...register(`tableData.${index}.count`)}
                  className={cls.textInput}
                />
              </TableCell>
              <TableCell>
                <input
                  placeholder="Add note"
                  type="text"
                  {...register(`tableData.${index}.note`)}
                  className={cls.textInput}
                />
              </TableCell>
              <TableCell>
                <input
                  placeholder="Add price"
                  type="text"
                  {...register(`tableData.${index}.price`)}
                  className={cls.textInput}
                />
              </TableCell>
              <TableCell>
                <input
                  placeholder="Add sum"
                  type="text"
                  {...register(`tableData.${index}.sum`)}
                  className={cls.textInput}
                />
              </TableCell>
              <TableCell onClick={() => remove(index)}>
                <DeleteIcon className={cls.deleteIcon} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ParcelTable;
