import SuperCard from "@/components/SuperCard/SuperCard";
import React from "react";
import cls from "./styles.module.scss";
import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";
import Table from "./Table";
import AddIcon from "@mui/icons-material/Add";
import SuperButton from "@/components/SuperButton/SuperButton";
import { useFieldArray, useForm } from "react-hook-form";

const data = {
  label: "",
  isChecked: false,
  count: 0,
  note: "",
  price: "",
  sum: "",
  id: 0,
};

const AddParcelPage = () => {
  const { handleSubmit, reset, watch, register, control } = useForm({
    defaultValues: {
      tracking_number: "",
      shop_name: "",
      receiver: "",
      tableData: [data],
    },
  });

  const { fields, append, prepend, remove, swap, move, insert } = useFieldArray(
    {
      control,
      name: "tableData",
    }
  );

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <form className={cls.addParcel} onSubmit={handleSubmit(onSubmit)}>
      <SuperCard title="Общая информация" header="Добавление посылки">
        <div className={cls.allInformation}>
          <div className={cls.inputWrapper}>
            <p>Номер трекинга</p>
            <input
              placeholder="Введите номер трекинга"
              name="tracking_number"
              onChange={() => console.log(watch("tracking_number"))}
              {...register("tracking_number")}
            />
          </div>
          <div className={cls.inputWrapper}>
            <p>Магазин</p>
            <input
              placeholder="Введите название магазина"
              name="shop_name"
              {...register("shop_name")}
            />
          </div>
          <div className={cls.inputWrapper}>
            <p>Получатель</p>
            <input placeholder="Введите название магазина" />
          </div>
        </div>
      </SuperCard>
      <SuperCard title="Декларация">
        <p className={cls.declarationDesc}>
          Во избежание проблем при таможенной очистке, просим вводить детальное
          описание наименования товара на русском.
        </p>
        <div className={cls.limit}>
          <CheckCircleRoundedIcon style={{ color: "#1AC19D" }} />
          <p>Лимит беспошлинного ввоза не превышен.</p>
        </div>
        <Table
          register={register}
          data={data}
          fields={fields}
          remove={remove}
        />
        <div
          className={cls.addMore}
          onClick={() => {
            console.log("clicked");
            append({ ...data, id: watch().tableData.at(-1) + 1 });
          }}
        >
          <AddIcon className={cls.addIcon} />
          <p>Добавить еще один</p>
        </div>

        <div className={cls.buttons}>
          <SuperButton text="Отменить" buttonType="secondary" />
          <SuperButton
            text="Добавить посылку"
            buttonType="primary"
            type="submit"
          />
        </div>
      </SuperCard>
    </form>
  );
};

export default AddParcelPage;
