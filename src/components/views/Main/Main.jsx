import { useEffect, useState } from "react"
import useTranslation from "next-translate/useTranslation"
import CustomSections from "../../UI/CustomSections/CustomSection"
import WhyShippo from "../../UI/Sections/WhyShippo/WhyShippo"
import BuyInsteadOfMe from "../../UI/Sections/BuyInsteadOfMe/BuyInsteadOfMe"
import Stores from "../../UI/Sections/Stores/Stores"
import DeliveryCount from "../../UI/Sections/DeliveryCount/DeliveryCount"
import SectionSwiper from "../../UI/Sections/SectionSwiper/SectionSwiper"
import ShippoService from "../../UI/Sections/ShippoService/ShippoService"
import classNames from "classnames"
import { usePost } from "@/services"
import styles from "./style.module.scss"

export function Main() {
  const { t } = useTranslation()


  const [active, setActive] = useState(false)
  // const { posts, createMutation } = usePost({
  //   params: { limit: 10, page: 1 },
  // })

  // const addPost = () => {
  //   createMutation.mutate(
  //     {
  //       data: JSON.stringify({
  //         title: "foo",
  //         body: "bar",
  //         userId: 1,
  //       }),
  //     },
  //     {
  //       onSuccess: (res) => {
  //         console.log(res)
  //       },
  //     }
  //   )
  // }

  return (
    <main
      className={classNames(styles.main, {
        [styles.active]: active,
      })}
    >
      <CustomSections changeColor={4}>
        <SectionSwiper />
      </CustomSections>
      <CustomSections changeColor={2}>
        <ShippoService />
      </CustomSections>
      <CustomSections changeColor={2}>
        <DeliveryCount />
      </CustomSections>
      <CustomSections changeColor={1}>
        <WhyShippo />
      </CustomSections>
      <CustomSections changeColor={2}>
        <BuyInsteadOfMe />
      </CustomSections>
      <CustomSections changeColor={2}>
        <Stores  />
      </CustomSections>

      {/* <Button size="large" color="primary" onClick={addPost}>
        Create post
      </Button>
      <div>
        {posts?.data?.map((item) => (
          <div key={item.id}>{item.title}</div>
        ))}
      </div> */}
    </main>
  )
}
