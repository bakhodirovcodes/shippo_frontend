import { useState } from "react"
import LeftSide from "./LeftSide/LeftSide"
import CustomSections from "@/components/UI/CustomSections/CustomSection"
import Modals from "@/components/UI/Modal/Modal"
import OtpForm from "./OtpForm/OtpForm"
import RegisterForm from "./RegisterForm/RegisterForm"
import styles from "./styles.module.scss"

const Register = () => {
  const [open, setOpen] = useState(false)
  const [openRegister, setOpenRegister] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleOpenRegister = () => setOpenRegister(true)
  const handleCloseRegister = () => setOpenRegister(false)
  return (
    <CustomSections>
      <div className={styles.container}>
        <div className={styles.leftSide}>
          <LeftSide
            handleOpen={handleOpen}
            outlinedText="У меня есть аккаунт"
            filledText="Создать аккаунт"
            resetPassword
            title="Создать аккаунт Shippo"
          />
          <Modals width="22%" open={open} handleClose={handleClose}>
            <OtpForm
              handleClose={handleClose}
              handleOpenRegister={handleOpenRegister}
            />
          </Modals>
          <Modals
            width="30%"
            open={openRegister}
            handleClose={handleCloseRegister}
          >
            <RegisterForm />
          </Modals>
        </div>
        <div className={styles.rightSide}></div>
      </div>
    </CustomSections>
  )
}

export default Register
