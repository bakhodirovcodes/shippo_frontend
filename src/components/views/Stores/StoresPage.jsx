import CustomSections from "@/components/UI/CustomSections/CustomSection"
import Magazine from "@/components/UI/Sections/Magazine/Magazine"

import styles from "./styles.module.scss"

const StoresPage = () => {
  return (
    <div className={styles.container}>
      <CustomSections changeColor={1}>
        <Magazine />
      </CustomSections>
    </div>
  )
}

export default StoresPage
