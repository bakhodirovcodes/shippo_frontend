import CustomSections from "@/components/UI/CustomSections/CustomSection"
import AccountFillingSection from "@/components/UI/Sections/AccountFillingSection/AccountFillingSection"
import styles from "./styles.module.scss"

const FillingAccount = () => {
  return (
    <CustomSections changeColor={1}>
      <AccountFillingSection />
    </CustomSections>
  )
}

export default FillingAccount
