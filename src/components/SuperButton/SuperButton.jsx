import React from "react";
import cls from "./styles.module.scss";

const SuperButton = ({ text, buttonType, color, ...props }) => {
  return (
    <button
      className={`${cls.button} ${cls?.[buttonType]} ${cls?.[color]} `}
      {...props}
    >
      {text}
    </button>
  );
};

export default SuperButton;
