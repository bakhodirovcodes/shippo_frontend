import { styled } from "@mui/material/styles"
import MuiTextField from "@mui/material/TextField"
import InputAdornment from "@mui/material/InputAdornment"
import MenuItem from "@mui/material/MenuItem"
import MuiPagination from "@mui/material/Pagination"
import PaginationItem from "@mui/material/PaginationItem"
import {
  DriveFileIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "public/icons/icons2"
import styles from "./styles.module.scss"

const TextField = styled((props) => (
  <MuiTextField
    select
    InputProps={{
      startAdornment: (
        <InputAdornment position="start">
          <DriveFileIcon />
        </InputAdornment>
      ),
      endAdornment: (
        <InputAdornment
          position="end"
          style={{
            position: "absolute",
            right: "7px",
            pointerEvents: "none",
          }}
        >
          <ChevronDownIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))({
  "& .MuiOutlinedInput-root": {
    padding: "0 0.375rem 0 0.75rem",
    borderRadius: "0.375rem",
    "& fieldset": {
      borderColor: "#E5E9EB",
    },
    "&:hover fieldset": {
      borderColor: "#E5E9EB",
    },
    "&.Mui-focused fieldset": {
      borderWidth: "1px",
      borderColor: "#E5E9EB",
    },
    "& .MuiSelect-select": {
      padding: "0.375rem 2rem 0.375rem 0",
      fontWeight: 500,
      fontSize: "0.875rem",
      lineHeight: "1.5rem",
    },
    "& .MuiSvgIcon-root": {
      display: "none",
    },
  },
})

const Pagination = styled((props) => (
  <MuiPagination
    shape="rounded"
    renderItem={(item) => (
      <PaginationItem
        components={{ previous: ChevronLeftIcon, next: ChevronRightIcon }}
        {...item}
      />
    )}
    {...props}
  />
))({
  "& .MuiButtonBase-root": {
    fontSize: "0.875rem",
    lineHeight: "1.5rem",
    color: "#252C32",
    "&.Mui-selected": {
      color: "#EF722E",
      borderRadius: "0.375rem",
      backgroundColor: "rgba(239, 114, 46, 0.08);",
    },
    "&:hover": {
      backgroundColor: "rgba(239, 114, 46, 0.08);",
    },
  },
})

const limits = [
  {
    id: "limit-01",
    value: 10,
    label: "Показать по 10",
  },
  {
    id: "limit-02",
    value: 20,
    label: "Показать по 20",
  },
  {
    id: "limit-03",
    value: 30,
    label: "Показать по 30",
  },
  {
    id: "limit-04",
    value: 40,
    label: "Показать по 40",
  },
  {
    id: "limit-05",
    value: 50,
    label: "Показать по 50",
  },
]

function ProfilePagination({ count = 10 }) {
  return (
    <div className={styles.pagination}>
      <TextField defaultValue="10">
        {limits.map((option) => (
          <MenuItem key={option.id} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>

      <Pagination count={count} />
    </div>
  )
}

export default ProfilePagination
