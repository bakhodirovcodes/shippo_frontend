import styles from "./styles.module.scss"

const OrderCard = (props) => {
  const { icon, title, component } = props

  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <div className={styles.icon}>{icon}</div>
        <h1 className={styles.title}>{title}</h1>
      </div>
      <div>{component}</div>
    </div>
  )
}

export default OrderCard
