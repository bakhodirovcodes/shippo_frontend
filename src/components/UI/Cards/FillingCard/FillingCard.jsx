import { useForm } from "react-hook-form"
import { useRouter } from "next/router"
import Information from "./Information/Information"
import Consent from "./Consent/Consent"
import OutlinedButton from "../../Buttons/OutlinedBtn"
import MuiCheckbox from "@mui/material/Checkbox"
import { styled } from "@mui/material/styles"
import useObjects from "@/services/getObjectList"
import { useOrderMutation } from "@/services/orderService"

import styles from "./styles.module.scss"

const Checkbox = styled(MuiCheckbox)({
  color: "#B0BABF",
  "&.Mui-checked": {
    color: "#EF722E",
  },
})

const FillingCard = () => {
  const router = useRouter()
  const { insert_link, product } = router.query

  const { handleSubmit, control } = useForm({
    defaultValues: {
      price_product: "",
      amount: "",
    },
  })

  const { mutate: create } = useOrderMutation({
    onSuccess: (res) => {
      console.log(res)
    },
  })

  const onSubmit = (data) => {
    create({ data: { ...data, insert_link: insert_link, product: product } })
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={styles.container}>
      <Information control={control} />
      <Consent control={control} />
      <div className={styles.buttons}>
        <div className={styles.aggreement}>
          <Checkbox />{" "}
          <span>
            Согласен с <a>Условиями услуги.</a>
          </span>
        </div>
        <OutlinedButton
          type="submit"
          title="Подтвердить заказ"
          // onClick={() => router.push("/")}
          className={styles.contained}
          textCLassName={styles.text}
        />
      </div>
    </form>
  )
}

export default FillingCard
