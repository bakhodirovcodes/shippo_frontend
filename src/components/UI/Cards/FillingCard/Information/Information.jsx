import Frow from "@/components/UI/FormElements/FRow"
import CTextField from "@/components/UI/FormElements/CTextField"
import CSelect from "@/components/UI/FormElements/CSelect"
import useObjects from "@/services/getObjectList"
import styles from "./styles.module.scss"

const Information = ({ control }) => {
  const { object } = useObjects({
    table_slug: "user",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  const allusers = []

  object?.data?.data?.response?.map((el) =>
    allusers.push({
      label: el.name,
      value: el.guid,
    })
  )
  return (
    <div className={styles.container}>
      <p className={styles.title}>Информация о товаре</p>
      <div className={styles.row1}>
        <Frow label="Цена товара" required>
          <CTextField
            fullWidth
            control={control}
            name="price_product"
            placeholder="В лирах"
            required
          />
        </Frow>
        <Frow label="Количество" required>
          <CTextField
            fullWidth
            control={control}
            name="amount"
            placeholder="В лирах"
            required
          />
        </Frow>
        <Frow label="Размер">
          <CTextField
            fullWidth
            control={control}
            name="size"
            placeholder="Если имеется"
          />
        </Frow>
        <Frow label="Цвет">
          <CTextField
            fullWidth
            control={control}
            name="color"
            placeholder="Цвет товара"
          />
        </Frow>
        <Frow label="Цена доставки">
          <CTextField
            fullWidth
            control={control}
            name="price_delivery"
            placeholder="В лирах"
          />
        </Frow>
      </div>
      <div className={styles.row2}>
        <Frow label="Получатель">
          <CSelect
            fullWidth
            options={allusers}
            control={control}
            name="recipient"
          />
        </Frow>
        <Frow label="Комментарии">
          <CTextField
            fullWidth
            control={control}
            name="commnet"
            placeholder="Комментарии"
          />
        </Frow>
      </div>
    </div>
  )
}

export default Information
