import CRadioButton from "@/components/UI/FormElements/CRadioButton"
import styles from "./styles.module.scss"

const radioLabels = ["Да", "Нет"]

const Consent = ({ control }) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>
        Согласны ли вы на покупку, если цена колеблется до 7%?
      </h1>
      <p className={styles.text}>
        В интернет-магазинах цены товаров могут колебаться (в среднем 0.1 -
        0.2%). Отметив да, вы соглашаетесь, что в таком случае разница в
        стоимости будет взиматься с вашего счета (но не более 7%) и покупка
        будет совершена.
      </p>
      <div className={styles.radioBtn}>
        <CRadioButton radioLabels={radioLabels} control={control} />
      </div>
    </div>
  )
}

export default Consent
