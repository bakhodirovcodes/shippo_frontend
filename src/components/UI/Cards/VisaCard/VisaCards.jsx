import styles from "./cards.module.scss"

const VisaCards = (props) => {
  const { icons } = props

  return <div className={styles.card}>{icons}</div>
}

export default VisaCards
