import styles from "./styles.module.scss"

const ShopCards = (props) => {
  const { image, title, text } = props
  return (
    <div className={styles.container}>
      <div className={styles.container__leftImage}>
        <img src={image} alt="shopImage" />
      </div>
      <div className={styles.container__rightContent}>
        <h1 className={styles.title}>{title}</h1>
        <div className={styles.reyting}>
          <span className={styles.reytingTitle}>Рейтинг:</span>
          <span className={styles.reytingNumber}>4.9</span>
          <p className={styles.reytingText}>Добавьте оценку:</p>
        </div>
        <p className={styles.text}>{text}</p>
      </div>
    </div>
  )
}

export default ShopCards
