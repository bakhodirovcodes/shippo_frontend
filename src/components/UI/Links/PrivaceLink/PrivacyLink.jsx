import CustomSections from "../../CustomSections/CustomSection"
import PrivacyPoliceSection from "../../Sections/PrivacyPoliceSection/PrivacyPoliceSection"
const PrivacyLink = () => {
  return (
    <>
      <CustomSections changeColor={1}>
        <PrivacyPoliceSection />
      </CustomSections>
    </>
  )
}

export default PrivacyLink
