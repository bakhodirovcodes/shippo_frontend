import CustomSections from "../../CustomSections/CustomSection"
import TermSection from "../../Sections/TermsSection/TermSection"
import styles from "./styles.module.scss"

const TermsLink = () => {
  return (
    <>
      <CustomSections changeColor={1}>
        <TermSection />
      </CustomSections>
    </>
  )
}

export default TermsLink
