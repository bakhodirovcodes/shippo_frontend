import styles from "./styles.module.scss"
import useObjects from "@/services/getObjectList"

const Brend = () => {
  const { object } = useObjects({
    table_slug: "about_us",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h1>{object?.data?.data?.response[1]?.title}</h1>
        <p>{object?.data?.data?.response[1]?.description}</p>
      </div>
      <div className={styles.image}>
        <img src={object?.data?.data?.response[1]?.foto} alt="brend" />
      </div>
    </div>
  )
}

export default Brend
