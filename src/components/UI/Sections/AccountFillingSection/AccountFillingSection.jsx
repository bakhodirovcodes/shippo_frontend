import FillingCard from "../../Cards/FillingCard/FillingCard"

import styles from "./styles.module.scss"

const AccountFillingSection = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Заполнение аккаунта</h1>
      <FillingCard />
     
    </div>
  )
}

export default AccountFillingSection
