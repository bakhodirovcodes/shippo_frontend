import useTranslation from "next-translate/useTranslation"
import Button from "../../Buttons/Button"
import styles from "./styles.module.scss"

const BuyInsteadOfMe = () => {
  const { t } = useTranslation()

  return (
    <div className={styles.container}>
      <div className={styles.image}>
        <img src="/images/BuyInsteadOfMe.png" alt="sectionImage" />
      </div>
      <div className={styles.info}>
        <h1>Используйте наш сервис Купи вместо меня</h1>
        <p>
          Если вы не можете сделать покупку самостоятельно, ничего страшного, мы
          вам поможем, просто нажмите кнопку Купи меня. Это экономит ваше время
        </p>
        <Button title="buyInsteadOfMe" changeBorder={true} />
      </div>
    </div>
  )
}

export default BuyInsteadOfMe
