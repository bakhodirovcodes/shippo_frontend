import { CardIcon, TimeIcon, TurkishCardIcon } from "/public/icons/icons"

export const adventureData = [
  {
    id: 1,
    icon: <CardIcon />,
    title: "Покупки при отсутствии банковской карты",
  },
  {
    id: 2,
    icon: <TimeIcon />,
    title: "Экономия времени",
  },
  {
    id: 3,
    icon: <TurkishCardIcon />,
    title: "Покупки в специальных магазинах через американскую карту",
  },
]
