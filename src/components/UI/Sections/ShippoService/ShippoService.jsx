import WaveCard from "../../Cards/Wavecard/WaveCard"
import { LoginKeyIcon, ShoppingIcon, ParcelIcon } from "/public/icons/icons"
import Button from "../../Buttons/Button"

import styles from "./styles.module.scss"

const texts = {
  1: "Зарегистрируйтесь, чтобы получить адреса для шоппинга, по которым интернет-магазины будут доставлять ваши заказы",
  2: "Покупайте в интернет-магазинах. Оформляя заказ, указывайте адрес для шоппинга (то есть адрес нашего склада).",
  3: "В личном кабинете добавьте отправление, из которого вам будет удобно его забрать или доставку курьером.",
}
const ShippoService = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.setviceTitle}>
        Чтобы воспользоваться сервисом Shippo, достаточно трёх простых шагов:
      </h1>
      <div className={styles.image}>
        <div className={styles.registerShippo}>
          <WaveCard
            icon={<LoginKeyIcon />}
            title="Регистрируетесь в Shippo"
            text={texts[1]}
          />
        </div>
        <div className={styles.order}>
          <WaveCard
            icon={<ShoppingIcon />}
            title="Делаете заказ"
            text={texts[2]}
          />
        </div>
        <div className={styles.parcel}>
          <WaveCard
            icon={<ParcelIcon />}
            title="Регистрируете посылку"
            text={texts[3]}
          />
          <Button changeBorder={true} title="Начать шоппинг" />
        </div>
      </div>
    </div>
  )
}
export default ShippoService
