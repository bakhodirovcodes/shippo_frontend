import { useForm } from "react-hook-form"
import Frow from "@/components/UI/FormElements/FRow"
import CTextField from "@/components/UI/FormElements/CTextField"
import CSelect from "@/components/UI/FormElements/CSelect"
import OutlinedButton from "@/components/UI/Buttons/OutlinedBtn"
import { useCommentMutation } from "@/services/commentService"
import styles from "./styles.module.scss"

const thems = [
  {
    label: "Предложение",
    value: "Предложение",
  },
  {
    label: "Жалоба",
    value: "Жалоба",
  },
  {
    label: "Благодарность",
    value: "Благодарность",
  },
]

const QualityFom = () => {
  const { control, handleSubmit } = useForm({
    defaultValues: {
      name: "",
    },
  })

  const { mutate: create } = useCommentMutation({
    onSuccess: (res) => {
      console.log(res)
    },
  })

  const onSubmit = (data) => {
    create({ data: { ...data } })
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={styles.container}>
      <h1 className={styles.title}>Контроль качества</h1>
      <div className={styles.row1}>
        <Frow label="Имя" required>
          <CTextField
            fullWidth
            control={control}
            name="name"
            placeholder="Имя"
          />
        </Frow>
        <Frow label="Тема" required>
          <CSelect fullWidth control={control} name="topic" options={thems} />
        </Frow>
      </div>
      <Frow label="Электронная почта" required>
        <CTextField
          fullWidth
          control={control}
          name="email"
          placeholder="Электронная почта"
        />
      </Frow>
      <CTextField
        fullWidth
        control={control}
        name="description"
        multiline
        rows={4}
        placeholder="Текст сообщения"
      />
      <OutlinedButton
        type="submit"
        className={styles.btn}
        textCLassName={styles.text}
        title="Отправить"
      />
    </form>
  )
}

export default QualityFom
