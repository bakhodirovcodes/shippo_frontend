import styles from "./stores.module.scss"

const StoresCards = (props) => {
  const { images, texts } = props
  return (
    <div className={styles.card}>
      <div><img src={images} alt="image" /></div>
      <p>{texts}</p>
    </div>
  )
}

export default StoresCards
