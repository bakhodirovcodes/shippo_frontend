export const storesItem = [
  {
    id: 1,
    image: <img src="/images/storesImages/oysho.png" />,
    title: "Oysho",
  },
  {
    id: 2,
    image: <img src="/images/storesImages/avva.png" />,
    title: "Avva",
  },
  {
    id: 3,
    image: <img src="/images/storesImages/adl.png"  />,
    title: "ADL",
  },
  {
    id: 4,
    image: <img src="/images/storesImages/lc.png" />,
    title: "LC Waikiki",
  },
  {
    id: 5,
    image: <img src="/images/storesImages/lacoste.png" />,
    title: "Lacoste",
  },
  {
    id: 6,
    image: <img src="/images/storesImages/pull.png" />,
    title: "Pull & Bear",
  },
  {
    id: 7,
    image: <img src="/images/storesImages/beymen.png" />,
    title: "Beymen",
  },
  {
    id: 8,
    image: <img src="/images/storesImages/adidas.png" />,
    title: "Adidas",
  },
]
