export const accordionData = [
  {
    id: 1,
    title: "Регистрация",
  },
  {
    id: 2,
    title: "Оформление заказа",
  },
  {
    id: 3,
    title: "Доставка",
  },
  {
    id: 4,
    title: "Доставка на дом",
  },
  {
    id: 5,
    title: "Налоги и таможенные платежи",
  },
]