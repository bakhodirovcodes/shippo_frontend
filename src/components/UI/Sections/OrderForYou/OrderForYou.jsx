import OrderCard from "../../Cards/OrderCard/OrderCard"
import { ordersData } from "./ordersData"
import styles from "./styles.module.scss"

const OrderForYou = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Shippo осуществит покупку вместо Вас</h1>
      <div className={styles.orders}>
        {ordersData.map((el) => (
          <>
            <OrderCard
              key={el.id}
              icon={el.icon}
              title={el.title}
              component={el.component}
            />
          </>
        ))}
      </div>
    </div>
  )
}

export default OrderForYou
