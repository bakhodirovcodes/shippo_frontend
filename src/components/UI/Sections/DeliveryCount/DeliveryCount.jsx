import { styled } from "@mui/material/styles"
import InputAdornment from "@mui/material/InputAdornment"
import { TextField } from "@mui/material"
import Switch from "@mui/material/Switch"
import styles from "./styles.module.scss"
import { useState } from "react"

const DeliveryCount = () => {
  const [statusState, setStatusState] = useState(true)

  const CTextField = styled(TextField)({
    "& label.Mui-focused": {
      color: "#B0BABF",
    },

    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#B0BABF",
      },
      "&:hover fieldset": {
        borderColor: "#B0BABF",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#B0BABF",
      },
    },
  })

  const IOSSwitch = styled((props) => (
    <Switch
      focusVisibleClassName=".Mui-focusVisible"
      disableRipple
      {...props}
    />
  ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    "& .MuiSwitch-switchBase": {
      padding: 0,
      margin: 2,
      transitionDuration: "300ms",
      "&.Mui-checked": {
        transform: "translateX(16px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          backgroundColor:
            theme.palette.mode === "dark" ? "#1AC19D" : "#1AC19D",
          opacity: 1,
          border: 0,
        },
        "&.Mui-disabled + .MuiSwitch-track": {
          opacity: 0.5,
        },
      },
      "&.Mui-focusVisible .MuiSwitch-thumb": {
        color: "#1AC19D",
        border: "6px solid #fff",
      },
      "&.Mui-disabled .MuiSwitch-thumb": {
        color:
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: theme.palette.mode === "light" ? 0.7 : 0.3,
      },
    },
    "& .MuiSwitch-thumb": {
      boxSizing: "border-box",
      width: 22,
      height: 22,
    },
    "& .MuiSwitch-track": {
      borderRadius: 26 / 2,
      backgroundColor: theme.palette.mode === "light" ? "#E9E9EA" : "#39393D",
      opacity: 1,
      transition: theme.transitions.create(["background-color"], {
        duration: 500,
      }),
    },
  }))

  return (
    <div className={styles.container}>
      <div className={styles.counter}>
        <div className={styles.weght}>
          <CTextField
            label="Вес"
            defaultValue="0"
            type="number"
            sx={{ m: 1, width: "100%" }}
            InputProps={{
              endAdornment: <InputAdornment position="end">кг</InputAdornment>,
            }}
          />
          <CTextField
            label="Ценность товара"
            defaultValue="0"
            type="number"
            sx={{ m: 1, width: "100%" }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">turk lirasi</InputAdornment>
              ),
            }}
          />
        </div>
        <div className={styles.cargoSwitch}>
          <p>Крупногабаритные грузы</p>
          <IOSSwitch
            onChange={() => setStatusState((prevCheck) => !prevCheck)}
            value={statusState}
            checked={!!statusState}
            defaultChecked
          />
        </div>
        <div className={styles.cargoSwitch}>
          <p>Жидкость</p>
          <IOSSwitch />
        </div>
       {statusState && <div className={styles.sizes}>
          <CTextField
            label="Высота"
            defaultValue="0"
            type="number"
            sx={{ m: 1, width: "100%" }}
            InputProps={{
              endAdornment: <InputAdornment position="end">см</InputAdornment>,
            }}
          />
          <CTextField
            label="Длина"
            defaultValue="0"
            type="number"
            sx={{ m: 1, width: "100%" }}
            InputProps={{
              endAdornment: <InputAdornment position="end">см</InputAdornment>,
            }}
          />
          <CTextField
            label="Ширина"
            defaultValue="0"
            type="number"
            sx={{ m: 1, width: "100%" }}
            InputProps={{
              endAdornment: <InputAdornment position="end">см</InputAdornment>,
            }}
          />
        </div>}
        <div className={styles.border}></div>
        <div className={styles.costs}>
          <p>Стоимость экспресс-доставки</p>
          <p className={styles.number}>0 $</p>
        </div>
        <div className={styles.costs}>
          <p>По курсу НБУ на сегодня</p>
          <p className={styles.number}>0 uzs</p>
        </div>
      </div>
    </div>
  )
}

export default DeliveryCount
