import styles from "./styles.module.scss"

const ContentRu = () => {
  return (
    <div className={styles.container}>
      <p>Глава 1. ОБЩИЕ УСЛОВИЯ</p>
      <p>
        1. Ниже перечисленные положения и условия определяют и регулируют
        правоотношения, возникающие у Сторон при предоставлении услуг
        предусмотренных веб-сайтом (далее Сайт) компании Глоббинг (далее
        Компания, Мы, Глоббинг). Компания предоставляет услуги как в Америке,
        также в других странах с помощью своих партнеров.
      </p>
      <p>
        2. Осуществляя вход на Сайт Компании, пользуясь любыми услугами Сайта
        или загружая как Пользователь (далее Пользователь, Вы, Вам, Ваш, Клиент)
        любые сведения о себе полностью или частично, а также иную информацию
        необходимое для оказания услуги, Вы подтверждаете, что полностью
        прочитали, ознакомлены и соглашаетесь с условия пользования Сайтом, так
        же с размещенными на Сайте условиями предоставления услуг, а также
        положениями публичного агенсткого договора (далее Условия или Договор) и
        безоговорочно принимаете их, несмотря на то, что Вы клиент,
        зарегистрированный пользователь или посетитель Сайта.
      </p>
      <p>
        3. Компания оставляет за собой право без предварительного уведомления, в
        одностороннем порядке, в любой момент изменить условия предусмотренные
        Договором. При изменении Условий Компания размещает на Сайте новые
        условия, при этом отметив дату последних внесенных изменений. Кроме
        этого, Компания не обязана уведомлять Пользователей Сайта о вносимых или
        внесенных изменениях, которое является обязанностью Пользователя, в
        связи с чем последний обязуется, самостоятельно посещать сайт и
        проверять изменения и актуальность Условий. Новые условия будут иметь
        юридическую силу и будут распространяться на обязательства Сторон со дня
        размещения на Сайте.
      </p>
      <p>
        4. В случае не согласия Пользователя с перечисленными Условиями,
        Пользователь вправе отказаться от использования Сайта. Пребывание на
        сайте, открытие счета, регистрация в качестве Пользователя и
        использование услуг Компании, является Вашим согласием (Акцептом) и
        подтверждением, , что вы безоговорочно принимаете эти Условия.
      </p>
      <p>Глава 2. ДЕЕСПОСОБНОСТЬ ПРИ ПОДПИСАНИИ ДОГОВОРА.</p>
      <p>
        5. Соглашаясь с Условиями, вы подтверждаете, что в соответствии с
        Законами Вашей страны вы полностью дееспособны и имеете право
        пользоваться Услугами. Если по Законам Вашей страны Вы являйтесь
        ограниченно дееспособным и не можете самостоятельно пользоваться
        услугами, то Вы должны получить согласие Ваших родителей или других
        компетентных лиц, чтобы пользоваться Услугами Компании. В случае, если
        Вы представляете интересы юридического лица, то Вы обязаны удостоверить,
        наличие документов подтверждающие Ваши полномочия, предоставляемые
        юридическим лицом,а также пользоваться услугами в соответствии с
        Законами страны, где зарегистрировано юридическое лицо. Соглашаясь с
        настоящими Условиями, Вы принимайте во внимание, что Мы не имеем
        возможности проверить информацию и сведения предоставленные Вами на
        подтверждение их подлинности , в связи с чем, при любом нарушении с
        Вашей стороны данного пункта всю ответственность несете самостоятельно.
      </p>
      <p>
        6. Право иностранных физических лиц на пользование Услугами Глоббинг: в
        соответствии с Кодексом РК «О таможенном регулировании в Республике
        Казахстан», иностранное физическое лицо может выступать декларантом в
        отношении товаров для личного пользования только при документальном
        подтверждении своего постоянного или временного проживания на территории
        Республики Казахстан.
      </p>
      <p>
        7. Перечень документов, один из которых обязано предоставить иностранное
        физическое лицо для таможенного оформления:
      </p>
      <p>l Вид на жительство</p>
      <p>l Удостоверение беженца</p>
      <p>l Справка о временной регистрации иностранца</p>
      <p>l Удостоверение лица без гражданства</p>
      <p>l Виза, выданное загранучреждениями Республики Казахстан.</p>
      <p>
        В случае отсутствия указанных документов, Глоббинг оставляет за собой
        право отказа в предоставлении услуг и таможенном оформлении товара. При
        этом, при превышении порога на беспошлинный ввоз товара для личного
        пользования, иностранное физическое лицо обязуется предоставить
        Глоббингу индивидуальный идентификационный номер (ИИН) присвоенный на
        территории Республики Казахстан.
      </p>
    </div>
  )
}

export default ContentRu
