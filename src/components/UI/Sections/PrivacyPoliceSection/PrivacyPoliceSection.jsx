import PrivacyContentRu from "./PrivacyContentRu/PrivacyContentRu"
import styles from "./styles.module.scss"

const PrivacyPoliceSection = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Политика конфиденциальности</h1>
      <PrivacyContentRu />
    </div>
  )
}

export default PrivacyPoliceSection
