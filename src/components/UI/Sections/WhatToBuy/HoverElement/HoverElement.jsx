import { InfoRoundedIcon } from "/public/icons/icons"
import styles from "./hover.module.scss"

const HoverElement = () => {
  return (
    <div className={styles.container}>
      <div className={styles.info}>
        <span>
          <InfoRoundedIcon />
        </span>
        <span className={styles.text}>
          Проста скопируйте ссылку из магазина и вставьте сюда
        </span>
      </div>
      <div className={styles.image}>
        <img src="/images/hovereImage.png" alt="hoverImage" />
      </div>
    </div>
  )
}

export default HoverElement
