import WithShippoCard from "../../Cards/WithShippoCard/WithShippoCard"
import { shippodata } from "./shippodata"
import styles from "./styles.module.scss"

const WithShippo = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.mainTitle}>С Shippo вы экономите время и деньги</h1>
      <div className={styles.cards}>
        {shippodata.map((el) => (
          <WithShippoCard
            key={el.id}
            icon={el.icon}
            title={el.title}
            text={el.text}
          />
        ))}
      </div>
    </div>
  )
}

export default WithShippo
