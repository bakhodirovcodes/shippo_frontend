import ProhibitedCard from "../../Cards/ProhibitedCard/ProhibitedCard"
import useObjects from "@/services/getObjectList"
import { prohibitedData } from "./prohibitedData"

import styles from "./styles.module.scss"
const ProhibitedSection = () => {
  const { object } = useObjects({
    table_slug: "prohibited_product",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  return (
    <div className={styles.container}>
      <h1 className={styles.container__title}>Запрещенные товары</h1>
      {object?.data?.data?.response?.map((el) => (
        <ProhibitedCard
          key={el.guid}
          icon={el.icon}
          title={el.title}
          text={el.description}
        />
      ))}
    </div>
  )
}

export default ProhibitedSection
