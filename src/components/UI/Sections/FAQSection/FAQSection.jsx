import AccordionInfo from "../ImportantForDelivery/AccordionInfo/AccordionInfo"
import useObjects from "@/services/getObjectList"
import { accordionData } from "../ImportantForDelivery/accordionData"

import styles from "./styles.module.scss"

const FAQSection = () => {
  const { object } = useObjects({
    table_slug: "faq",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  const getChildrenRecursive = (list) => {
    let computed = []
    
  }

  console.log("accordion", getChildrenRecursive(object))
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Часто задаваемые вопросы</h1>
      {accordionData.map((el) => (
        <AccordionInfo key={el.id} title={el.title} />
      ))}
    </div>
  )
}

export default FAQSection
