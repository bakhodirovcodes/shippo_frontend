import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import Slider from "react-slick"
import { PrevICon, NextIcon } from "/public/icons/icons"
import Image from "next/image"
import useObjects from "@/services/getObjectList"
import styles from "./styles.module.scss"
import { Swiper, SwiperSlide } from "swiper/react"
import "swiper/css"
import "swiper/css/pagination"
import "swiper/css/navigation"
import { Pagination, Navigation } from "swiper"

const SectionSwiper = () => {
  const { object } = useObjects({
    table_slug: "landing_page",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  const length = object?.data?.data?.response?.length

  return (
    <div className={styles.sliderWrapper}>
      <Swiper
        slidesPerView={1}
        spaceBetween={length}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        onNavigationPrev={styles.prevIcon}
        onNavigationNext=".nextIcon"
        modules={[Pagination, Navigation]}
        className="custom-swiper"
      >
        {object?.data?.data?.response?.map((el) => (
          <SwiperSlide key={el.guid}>
            <img className={styles.banner} src={el.banner} alt="banner" />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}

export default SectionSwiper
