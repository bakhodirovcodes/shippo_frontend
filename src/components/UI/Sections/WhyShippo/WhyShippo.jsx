import useTranslation from "next-translate/useTranslation";
import Cards from "./Cards/Cards";
import { cardsItems } from "./data";
import Button from "@/components/UI/Buttons/Button";
import styles from "./styles.module.scss";

const WhyShippo = () => {
  const { t } = useTranslation();
  return (
    <div className={styles.container}>
      <div className={styles.leftSide}>
        <h1 className={styles.title}>{t("whyShippo")}</h1>
        <p className={styles.text}>
          В Турции 95 % интернет-магазинов не делают прямую доставку в
          Узбекистан. А Shippo позволяет совершать покупки в интернет-магазинах
          Турции и получать их по всему Узбекистану. С нашими инновационными и
          революционными идеям, а также технологиями мы делаем все для того,
          чтобы вы получили лучший опыт в шопинге, и самое главное свободу и
          были выше всех границ.
        </p>
        <Button
          className={styles.button}
          changeBorder={true}
          title="registration"
        />
      </div>
      <div className={styles.rightCards}>
        <div className={styles.cards}>
          {cardsItems.map((item) => (
            <Cards key={item.id} icons={item.icon} text={item.text} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default WhyShippo;
