import {
  InnovationIcon,
  StockIcon,
  DeleviryIcon,
  TaxIcon,
} from "/public/icons/icons"

export const cardsItems = [
  {
    id: 1,
    icon: <InnovationIcon />,
    text: "Инновационные сервис-центры",
  },
  {
    id: 2,
    icon: <StockIcon />,
    text: "Имеется sэкспресс-склады",
  },
  {
    id: 3,
    icon: <DeleviryIcon />,
    text: "Бесплатная доставка на дом для посылок +1 кг",
  },
  {
    id: 4,
    icon: <TaxIcon />,
    text: "0%  гос-налога с продаж",
  },
]
