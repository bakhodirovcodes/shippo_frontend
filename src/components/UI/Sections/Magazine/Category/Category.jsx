import CheckBoxLabel from "@/components/UI/FormElements/CheckBoxLabel"
import OutlinedButton from "@/components/UI/Buttons/OutlinedBtn"
import { categoryData } from "./categoryData"
import useObjects from "@/services/getObjectList"
import styles from "./category.module.scss"

const Category = () => {
  const { object } = useObjects({
    table_slug: "category",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  return (
    <div className={styles.container}>
      <h1 className={styles.container__title}>Категория</h1>
      <div className={styles.container__category}>
        {object?.data?.data?.response?.map((item) => (
          <CheckBoxLabel key={item.guid} label={item.title} />
        ))}
      </div>
      <OutlinedButton className={styles.btn} title="resetFilter" />
    </div>
  )
}

export default Category
