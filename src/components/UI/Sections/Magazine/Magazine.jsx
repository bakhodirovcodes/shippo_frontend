import Category from "./Category/Category"
import ShopCards from "../../Cards/Shop/ShopCards"
import { shopCardsData } from "./shopCardData"
import useObjects from "@/services/getObjectList"
import styles from "./styles.module.scss"

const Magazine = () => {
  const { object } = useObjects({
    table_slug: "contact_shops",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  return (
    <div className={styles.container}>
      <div className={styles.container__category}>
        <Category />
      </div>
      <div className={styles.container__shopCards}>
        {object?.data?.data?.response?.map((el) => (
          <ShopCards
            key={el.guid}
            image={el.photo}
            title={el.name}
            text={el.description}
          />
        ))}
      </div>
    </div>
  )
}

export default Magazine
