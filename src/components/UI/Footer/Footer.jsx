import Link from "next/link"
import { Container } from "@mui/material"
import useTranslation from "next-translate/useTranslation"
import {
  ResursIcon,
  LocationIcon,
  EmailIcon,
  PhoneIcon,
  FacebookIcon,
  TelegramIcon,
  InstagramIcon,
  WatsappIcon,
  TwitterIcon,
} from "/public/icons/icons"
import VisaCards from "../Cards/VisaCard/VisaCards"
import { cards } from "./cards"
import styles from "./style.module.scss"

export function Footer() {
  const { t } = useTranslation("common")
  return (
    <footer className={styles.footer}>
      <Container>
        <div className={styles.box}>
          <div className={styles.resurs}>
            <ResursIcon />
            <div className={styles.visaCards}>
              {cards.map((el) => (
                <VisaCards key={el.id} icons={el.icon} />
              ))}
            </div>
          </div>
          <div className={styles.links}>
            <ul className={styles.contacts}>
              <li>
                <Link href="/">
                  <a>
                    <LocationIcon />
                    <span>Ташкент, улица Пушкина 12</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <EmailIcon />
                    <span>info@shippo.uz</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <PhoneIcon />
                    <span>+7 727 344 14 64</span>
                  </a>
                </Link>
              </li>
            </ul>
            <ul className={styles.urls}>
              <li>
                <Link href="/faqlink">
                  <a className={styles.urlItems}>{t("faq")}</a>
                </Link>
              </li>
              <li>
                <Link href="/prohibited">
                  <a className={styles.urlItems}>{t("prohibitedItems")}</a>
                </Link>
              </li>
              <li>
                <Link href="/terms">
                  <a className={styles.urlItems}>Общие условия</a>
                </Link>
              </li>
              <li>
                <Link href="/privacy">
                  <a className={styles.urlItems}>Политика конфиденциальности</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className={styles.underline}></div>
        <div className={styles.footerText}>
          <p className={styles.info}>© 2023 Shippo ООО. Все права защищены</p>
          <ul>
            <li className={styles.networks}>
              <Link href="/">
                <a>
                  <FacebookIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <InstagramIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <TwitterIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <TelegramIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <WatsappIcon />
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </Container>
    </footer>
  )
}
