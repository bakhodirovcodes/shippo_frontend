import {
  MasterCardIcon,
  VisaCardIcon,
  UzcardIcon,
  UzumIcon,
  HumoIcon,
} from "/public/icons/icons"
export const cards = [
  {
    id: 1,
    icon: <MasterCardIcon />,
  },
  {
    id: 2,
    icon: <VisaCardIcon />,
  },
  {
    id: 3,
    icon: <UzcardIcon />,
  },
  {
    id: 4,
    icon: <MasterCardIcon />,
  },
  {
    id: 5,
    icon: <UzumIcon />,
  },
  {
    id: 6,
    icon: <HumoIcon />,
  },
  {
    id: 7,
    icon: <UzcardIcon />,
  },
]
