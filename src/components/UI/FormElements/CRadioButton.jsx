import React, { useState } from "react"
import { styled } from "@mui/material/styles"
import Radio from "@mui/material/Radio"
import { RadioGroup, Typography } from "@mui/material"
import { Controller } from "react-hook-form"
// import { CheckedRadioIcon, UncheckedRadioIcon } from "components/Icons/Icons"
import styles from "./styles.module.scss"

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        padding: "0",
        "&:hover": {
          bgcolor: "transparent",
        },
      }}
      disableRipple
      color="default"
      // checkedIcon={<CheckedRadioIcon />}
      // icon={<UncheckedRadioIcon />}
      {...props}
    />
  )
}

export default function CRadioButton({
  title,
  name = "",
  control,
  defaultValue,
  radioLabels,
  errors,
  validation,
}) {
  return (
    <>
      <Controller
        control={control}
        defaultValue={defaultValue}
        name={name}
        render={({ field: { value, onChange, errors } }) => {
          return (
            <>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                name="radio-buttons-group"
              >
                <div className={styles.radioWrapper}>
                  {radioLabels.map((label, index) => (
                    <div key={label} className={styles.labelButton}>
                      <BpRadio
                        value={label}
                        id={`label-${index}`}
                        onChange={onChange}
                      />{" "}
                      <label htmlFor={`label-${index}`}>
                        <span>{label}</span>
                      </label>
                    </div>
                  ))}
                </div>
              </RadioGroup>
            </>
          )
        }}
      />
    </>
  )
}
