import useTranslation from "next-translate/useTranslation"
import styles from "./styles.module.scss"

const OutlinedButton = (props) => {
  const {
    type = "button",
    title,
    onClick = () => {},
    className,
    textCLassName,
  } = props
  const { t } = useTranslation()

  return (
    <button
      type={type}
      className={className || styles.outlined}
      onClick={onClick}
    >
      {title && (
        <span className={textCLassName || styles.text}>{t(title)}</span>
      )}
    </button>
  )
}

export default OutlinedButton
