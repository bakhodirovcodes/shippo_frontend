import styles from "./register.module.scss"
import Frow from "@/components/UI/FormElements/FRow"
import CTextField from "@/components/UI/FormElements/CTextField"
import CSelect from "@/components/UI/FormElements/CSelect"

const gender = [
  {
    label: "Mужской",
    value: "Mужской",
  },
  {
    label: "Женский",
    value: "Женский",
  },
]

const RegisterInput = ({ control }) => {
  return (
    <div className={styles.container}>
      <Frow label="Имя">
        <CTextField
          fullWidth
          control={control}
          name="first_name"
          placeholder="Введите имя"
        />
      </Frow>
      <Frow label="Фамилия">
        <CTextField
          fullWidth
          control={control}
          name="last_name"
          placeholder="Введите фамилию"
        />
      </Frow>
      <Frow label="Отчество">
        <CTextField
          fullWidth
          control={control}
          name="patronimic"
          placeholder="Введите отчество"
        />
      </Frow>
      <Frow label="Номер телефона">
        <CTextField
          fullWidth
          control={control}
          name="phone"
          placeholder="Введите номер телефона"
        />
      </Frow>
      <Frow label="Год рождение">
        <CTextField
          type="date"
          fullWidth
          control={control}
          name="year_of_birth"
        />
      </Frow>
      <Frow label="Пол">
        <CSelect fullWidth options={gender} control={control} name="gander" />
      </Frow>
    </div>
  )
}

export default RegisterInput
