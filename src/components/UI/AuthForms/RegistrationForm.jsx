import styles from "./style.module.scss"
import OutlinedButton from "../Buttons/OutlinedBtn"
import { setCookie } from "nookies"
import { useForm } from "react-hook-form"
import { useRouter } from "next/router"
import RegisterInput from "./RegisterInput/RegisterInput"
import verifyEmailService from "@/services/Auth/veryfyEmail"

const RegistrationForm = ({ setOpenRegister, email }) => {
  const router = useRouter()
  const { handleSubmit, control } = useForm({
    defaultValues: {
      name: "",
    },
  })
  const onSubmit = (data) => {
    verifyEmailService
      .register({
        data: {
          login: email,
          phone: data.phone,
          name: data.first_name + " " + data.last_name + " " + data.patronimic,
          year_of_birth: data.year_of_birth,
          gander: data.gander,
          email: email,
        },
      })
      .then((res) => {
        setCookie(null, "token", res.data.data.token.access_token, {
          path: "/",
        })
        setCookie(null, "user_name", data.name, {
          path: "/",
        })
        setCookie(null, "user_id", res.data.data.user_id, {
          path: "/",
        })
        window.location.reload()
        // router.push("/personal-room");
      })
      .catch((err) => console.log(err))
    setOpenRegister(false)
  }

  return (
    <div className={styles.registerBox}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <h1 className={styles.title}>Общая информация</h1>
        <div className={styles.registerinputBox}>
          <RegisterInput control={control} />
          <div className={styles.buttons}>
            <OutlinedButton
              type="submit"
              className={styles.btn}
              textCLassName={styles.text}
              title="Сохранить"
            />
          </div>
        </div>
      </form>
    </div>
  )
}

export default RegistrationForm
