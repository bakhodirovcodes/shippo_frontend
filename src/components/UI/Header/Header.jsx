import { useState } from "react"
import { useRouter } from "next/router"
import { Container, Select, MenuItem } from "@mui/material"
import useTranslation from "next-translate/useTranslation"
import InputBase from "@mui/material/InputBase"
import { styled } from "@mui/material/styles"
import Headroom from "react-headroom"
import Link from "next/link"
import ProfilePopover from "../Cards/ProfilePopover/ProfilePopover"
import BonusPopover from "../Cards/BonusPopover/BonusPopover"
import {
  LogoIcon,
  CalculatorIcon,
  AlarmIcon,
  DollarIcon,
} from "/public/icons/icons"
import { profileData } from "./profileData"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import Modals from "../Modal/Modal"
import LoginForm from "../AuthForms/LoginForm"
import OTPForm from "../AuthForms/OTPForm"
import RegistrationForm from "../AuthForms/RegistrationForm"
import nookies from "nookies"
import Button from "../Buttons/Button"
import styles from "./style.module.scss"

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 100,
    position: "relative",
    backgroundColor: "#F6F8F9",
    border: "1px solid #F6F8F9",
    fontSize: 16,
    padding: "10px 12px 10px 16px",
    "&:focus": {
      borderRadius: 100,
    },
  },
}))

const Header = () => {
  const [responseSendSms, setResponseSendSms] = useState()
  const [email, setEmail] = useState()
  const [smsId, setSmsId] = useState()
  const [openCode, setOpenCode] = useState(false)
  const [openRegister, setOpenRegister] = useState(false)
  const [open, setOpen] = useState(false)
  const [age, setAge] = useState("")
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handlecloseCode = () => setOpenCode(false)
  const handleCloseRegiste = () => setOpenRegister(false)

  const { user_name } = nookies.get("user_name")
  const router = useRouter()
  const { t } = useTranslation()
  const langs = [
    {
      key: "ru",
      label: "Ру",
    },
    {
      key: "uz",
      label: "Uz",
    },
    {
      key: "en",
      label: "En",
    },
  ]
  const handleChange = (event) => {
    setAge(event.target.value)
  }

  return (
    <Headroom>
      <header className={styles.header}>
        <Container>
          <div className={styles.box}>
            <div className={styles.links}>
              <Link href="/">
                <a className={styles.logo}>
                  <LogoIcon />
                </a>
              </Link>
              <nav>
                <ul>
                  <li>
                    <Link href="/about">
                      <a>{t("about")}</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/stores">
                      <a>{t("shops")}</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/faq">
                      <a>{t("howItWorks")}</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/buyForMe">
                      <a>{t("buyInsteadOfMe")}</a>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className={styles.profile}>
              <Select
                displayEmpty
                id="demo-customized-select"
                value={age}
                onChange={handleChange}
                IconComponent={KeyboardArrowDownIcon}
                input={<BootstrapInput />}
              >
                <MenuItem value="" disabled>
                  Ру
                </MenuItem>
                {langs.map((el) => (
                  <MenuItem key={el.key} value={el.key}>
                    <Link href={router.asPath} locale={el.key}>
                      {el.label}
                    </Link>
                  </MenuItem>
                ))}
              </Select>
              {/* <div className={styles.calculator}>
                <CalculatorIcon />
              </div> */}
              <Button
                title="createAnAccount"
                onClick={handleOpen}
                // onClick={() => router.push("/profile/my-parcels/added")}
              />
              {user_name && (
                <div className={styles.alarm}>
                  <AlarmIcon />
                </div>
              )}
              {user_name && (
                <div className={styles.bonus}>
                  <DollarIcon />
                  <p>97 USD</p>
                  <div className={styles.bounsPopover}>
                    <BonusPopover />
                  </div>
                </div>
              )}
              {user_name && (
                <div className={styles.profileName}>
                  <p>M</p>
                  <div className={styles.profilePopover}>
                    {profileData.map((el) => (
                      <ProfilePopover
                        key={el.id}
                        icon={el.icon}
                        title={el.title}
                        number={el.number}
                        link={el.link}
                        onClick={el.onClick}
                      />
                    ))}
                  </div>
                </div>
              )}
            </div>
          </div>
        </Container>
        <Modals width="30%" open={open} handleClose={handleClose}>
          <LoginForm
            setResponseSendSms={setResponseSendSms}
            setOpenCode={setOpenCode}
            setOpen={setOpen}
            setSmsId={setSmsId}
            setEmail={setEmail}
          />
        </Modals>
        <Modals width="30%" open={openCode} handleClose={handlecloseCode}>
          <OTPForm
            setOpenCode={setOpenCode}
            setOpenRegister={setOpenRegister}
            responseSendSms={responseSendSms}
          />
        </Modals>
        <Modals
          width="40%"
          open={openRegister}
          handleClose={handleCloseRegiste}
        >
          <RegistrationForm
            setOpenRegister={setOpenRegister}
            setOpenCode={setOpenCode}
            responseSendSms={responseSendSms}
            smsId={smsId}
            email={email}
          />
        </Modals>
      </header>
    </Headroom>
  )
}

export default Header
