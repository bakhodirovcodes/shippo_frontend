import {
  CheckIcon,
  NotificationIcon,
  AddressICon,
  WalletIcon,
  UsersIcon,
  CogSettingsIcon,
  ExitIcon,
} from "/public/icons/icons"



export const profileData = [
  {
    id: 1,
    icon: <CheckIcon />,
    title: "Мои посылки",
    link: "/profile/my-parcels/added",
  },
  {
    id: 2,
    icon: <NotificationIcon />,
    title: "Уведомление",
    number: "16",
    link: "/profile/notification",
  },
  {
    id: 3,
    icon: <AddressICon />,
    title: "Мои адреса",
    link: "/profile/my-addresses",
  },
  {
    id: 4,
    icon: <WalletIcon />,
    title: "Баланс и транзакции",
    link: "/profile/balance-and-transactions",
  },
  {
    id: 5,
    icon: <UsersIcon />,
    title: "Получатели",
    link: "/profile/recipients",
  },
  {
    id: 6,
    icon: <CogSettingsIcon />,
    title: "Настройка",
    link: "/profile/setting",
  },
  {
    id: 7,
    icon: <ExitIcon />,
    title: "Выйти",
    link: "/",
  },
]
