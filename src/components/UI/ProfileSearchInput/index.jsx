import { styled } from "@mui/material/styles"
import MuiTextField from "@mui/material/TextField"
import InputAdornment from "@mui/material/InputAdornment"
import { SearchIcon } from "public/icons/icons2"

const TextField = styled((props) => (
  <MuiTextField
    InputProps={{
      startAdornment: (
        <InputAdornment position="start">
          <SearchIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))({
  width: "100%",
  "& .MuiOutlinedInput-root": {
    paddingLeft: "1rem",
    borderRadius: "0.5rem",
    backgroundColor: "#F5F5F5",
    "&.Mui-focused": {
      backgroundColor: "transparent",
    },
    "& input": {
      padding: "0.875rem 1rem 0.875rem 0",
      fontSize: "1rem",
      lineHeight: "1.1875rem",
    },
    "& fieldset": {
      borderColor: "transparent",
    },
    "&:hover fieldset": {
      borderColor: "transparent",
    },
    "&.Mui-focused fieldset": {
      borderWidth: "1px",
      borderColor: "#EF722E",
    },
  },
})

function ProfileSearchInput() {
  return <TextField placeholder="Поиск по товарам" />
}

export default ProfileSearchInput
