import PropTypes from "prop-types"
import { styled } from "@mui/material/styles"
import MuiTabs from "@mui/material/Tabs"
import MuiTab from "@mui/material/Tab"
import Box from "@mui/material/Box"

const Tabs = styled((props) => <MuiTabs {...props} />)({
  "& .MuiTabs-flexContainer": {
    columnGap: "1.25rem",
  },
  "& .MuiTabs-indicator": {
    height: "0.25rem",
    backgroundColor: "#141B2B",
  },
})

const Tab = styled((props) => <MuiTab {...props} />)({
  padding: "1rem 0.25rem",
  fontWeight: 500,
  fontSize: "0.875rem",
  color: "#84919A",
  lineHeight: "1.5rem",
  "&.Mui-selected": {
    color: "#141B2B",
    borderColor: "blue",
  },
})

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
}

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tabpanel-${index}`,
  }
}

export function ProfileTabs(props) {
  return (
    <Tabs
      value={props.value}
      onChange={props.handleChange}
      aria-label="table tabs"
      {...props}
    >
      {props.children}
    </Tabs>
  )
}

export function ProfileTab(props) {
  return <Tab label={props.label} {...a11yProps(props.a11yProps)} {...props} />
}

export function ProfileTabPanel(props) {
  return (
    <TabPanel value={props.value} index={props.index} {...props}>
      {props.children}
    </TabPanel>
  )
}
