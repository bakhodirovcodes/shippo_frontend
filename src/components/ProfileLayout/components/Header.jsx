import { styled } from "@mui/material/styles";
import MuiButton from "@mui/material/Button";
import { AddIcon } from "public/icons/icons2";
import styles from "./styles.module.scss";
import Link from "next/link";

const Button = styled(MuiButton)({
  padding: "0.5rem 1.5rem",
  backgroundColor: "#EF722E",
  boxShadow: "none",
  fontSize: "1rem",
  color: "#FFF",
  lineHeight: "1.5rem",
  "&:hover": {
    backgroundColor: "#EF722E",
  },
});

function Header() {
  return (
    <div className={styles.header}>
      <h1 className={styles.header__title}>Личный кабинет</h1>
      <Link href="/add-parcel">
        <a>
          <Button variant="contained" startIcon={<AddIcon />}>
            Добавить посылку
          </Button>
        </a>
      </Link>
    </div>
  );
}

export default Header;
