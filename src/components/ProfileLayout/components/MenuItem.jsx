import { useRouter } from "next/router"
import { styled } from "@mui/material/styles"
import MuiAccordion from "@mui/material/Accordion"
import MuiAccordionSummary from "@mui/material/AccordionSummary"
import MuiAccordionDetails from "@mui/material/AccordionDetails"
import classNames from "classnames"
import { ArrowDownIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))({
  "&:before": {
    display: "none",
  },
})

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowDownIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))({
  padding: 0,
  minHeight: "auto",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(180deg)",
  },
  "& .MuiAccordionSummary-content": {
    display: "flex",
    alignItems: "center",
    columnGap: "0.75rem",
    margin: 0,
    padding: "0.625rem 0.5rem",
  },
})

const AccordionDetails = styled(MuiAccordionDetails)(() => ({
  padding: "0 0 0 1.5rem",
}))

function MenuItem({ data }) {
  const router = useRouter()

  return data?.children ? (
    <Accordion
      defaultExpanded={data?.children?.some((i) =>
        router?.pathname?.match(i?.pathname)
      )}
    >
      <AccordionSummary aria-controls={data?.id} id={data?.id}>
        <span className={styles.menuItem__icon}>{<data.icon />}</span>
        <span className={styles.menuItem__title}>{data.title}</span>
      </AccordionSummary>
      <AccordionDetails>
        {data?.children?.map(({ id, title, pathname }) => (
          <div
            key={id}
            className={classNames(styles.menuItem__accordionDetails, {
              [styles.menuItem__accordionDetails_selected]:
                router.pathname.match(pathname),
            })}
            onClick={() => router.push(pathname)}
          >
            {title}
          </div>
        ))}
      </AccordionDetails>
    </Accordion>
  ) : (
    <div
      className={classNames(styles.menuItem, {
        [styles.menuItem_selected]: data?.pathname === router?.pathname,
      })}
      onClick={() => router.push(data.pathname)}
    >
      <span className={styles.menuItem__icon}>{<data.icon />}</span>
      <span className={styles.menuItem__title}>{data.title}</span>
    </div>
  )
}

export default MenuItem
