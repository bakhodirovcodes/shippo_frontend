import { Container } from "@mui/material"
import Header from "./components/Header"
import Sidebar from "./components/Sidebar"
import styles from "./styles.module.scss"

function ProfileLayout({ children }) {
  return (
    <div className={styles.profile}>
      <Container>
        <Header />

        <div className={styles.profile__wrapper}>
          <Sidebar />
          <div className={styles.profile__content}>{children}</div>
        </div>
      </Container>
    </div>
  )
}

export default ProfileLayout
