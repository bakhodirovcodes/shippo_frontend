import React from "react";
import cls from "./styles.module.scss";

const EmptyBox = ({ p }) => {
  return (
    <div className={cls.main} style={{ padding: p }}>
      <img src="/icons/emptyBox.svg" className={cls.boxIcon} />
    </div>
  );
};

export default EmptyBox;
