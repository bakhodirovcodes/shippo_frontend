import { useMutation } from "react-query"
import { request } from "./http-client"

const buyOrderForMe = {
  create: (data) => request.post(`/v1/object/buy_order_for_me`, data),
}

export const useOrderMutation = (mutationSettings) => {
  return useMutation((data) => buyOrderForMe.create(data), mutationSettings)
}
