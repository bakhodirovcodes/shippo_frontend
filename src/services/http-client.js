import axios from "axios"
import nookies from "nookies"
import { QueryClient } from "react-query"

const { token } = nookies.get("token")

export const request = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL,
  headers: {
    "x-api-key": "P-ay62PT3mB9Li1JflbRvBAFCaCP3fdolE",
    "platform-type": "super-admin",
    authorization: token ? `Bearer ${token}` : "API-KEY",
  },
})

const errorHandler = (error) => {
  return Promise.reject(error.response)
}

request.interceptors.response.use((response) => response.data, errorHandler)

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
})
