import { request } from "./http-client"

const objectService = {
  getByIdUser: (id, data) => request.get(`/v1/object/user/${id}`, data),
}

export default objectService
